<?php

namespace Drupal\connect2;

use Drupal\Component\Serialization\Json;
use Drupal\Core\URL;
use Drupal\Core\Link;
use GuzzleHttp\Exception\GuzzleException;

class Connect2Client {
  /**
   * @var \GuzzleHttp\Client
   */
  protected $client;
  protected $account_api;

  /**
   * Connect2 constructor
   *
   * @param $http_client_factory \Drupal\Core]\Http\ClientFactory
   */
  public function __construct($http_client_factory) {
    $config = \Drupal::config('connect2_facility_count.settings');

    $base_uri = $config->get('url');
    $account = $config->get('account');

    if ($base_uri) {
      $this->client = $http_client_factory->fromOptions([
        'base_uri' => $base_uri
      ]);
    } else {
      $this->c2_must_configure();
    }

    if ($account) {
      $this->account_api = $account;
    } else {
      $this->c2_must_configure();
    }
  }

  /**
   * Set error message if Connect2 is not configured
   */
  public function c2_must_configure() {
    // Get URL from Routing for configuration page
    $url = URL::fromRoute('connect2.config');

    // Set error message for user to set URL
    \Drupal::messenger()->addError(
      t('The Connect2 URL or Account API has not been %configured.',
      [ '%configured' => Link::fromTextAndUrl('configured', $url)->toString() ]
    ));
  }

  /**
   * Get Facility Options
   */
  public function getFacilityOptions() {
    $options = [];

    try {
      $url = "/api/FacilityCount/GetCountsByAccount" . "?AccountAPIKey=" . $this->account_api;
      $response = $this->client->get($url);

      $response = Json::decode($response->getBody());

      foreach ($response as $facility) {
        foreach ($response as $facility) {
          $value = $facility['FacilityId'];
          $display = $facility['FacilityName'];
          $options[$value] = $display;
        }
      }

      // Alphabetically sort the facilities
      asort($options);
      return $options;
    }
    catch (GuzzleException $e) {
      \Drupal::logger('type')->error($e->getMessage());
      return $options;
    }
  }

  /**
   * Get Location Options
   */
  public function getLocationOptions() {
    $options = [];

    try {
      $url = "/api/FacilityCount/GetCountsByAccount" . "?AccountAPIKey=" . $this->account_api;
      $response = $this->client->get($url);

      $response = Json::decode($response->getBody());

      foreach ($response as $facility) {
        $value = $facility['LocationId'];
        $display = $facility['LocationName'] . ' <em>(' . $facility['FacilityName'] . ')</em>';
        $options[$value] = $display;
      }

      // Alphabetically sort the locations
      asort($options);
      return $options;
    }
    catch (GuzzleException $e) {
      \Drupal::logger('type')->error($e->getMessage());
      return $facilities;
    }
  }

  /**
   * Get Facility Count
   */
  public function getFacilityCount($facility_filter, $location_filter, $closed_filter) {
    $facilities = [];

    try {
      $url = "/api/FacilityCount/GetCountsByAccount" . "?AccountAPIKey=" . $this->account_api;
      $response = $this->client->get($url);

      $response = Json::decode($response->getBody());

      foreach ($response as $facility) {
        // Filter by Closed
        if ($closed_filter == 0 && $facility['IsClosed']) {
          continue;
        } else {
          // Filter by Facility
          if (!empty($facility_filter)
            && !in_array($facility['FacilityId'], $facility_filter)) {
            continue;
          } else {
            // Filter by Location
            if (!empty($location_filter)
              && !in_array($facility['LocationId'], $location_filter)) {
              continue;
            } else {
              $percentage = ($facility['LastCount']/$facility['TotalCapacity'])*100;

              $facilities[$facility['FacilityName']][$facility['LocationName']] = [
                'closed' => $facility['IsClosed'],
                'capacity' => $facility['TotalCapacity'],
                'count' => $facility['LastCount'],
                'percentage' => strval($percentage),
                'updated' => $facility['LastUpdatedDateAndTime'],
              ];
            }
          }
        }
        // Alphabetically sort by Location
        ksort($facilities[$facility['FacilityName']]);
      }

      // Alphabetically sort by Facility
      ksort($facilities);
      return $facilities;
    }
    catch (GuzzleException $e) {
      \Drupal::logger('type')->error($e->getMessage());
      return $facilities;
    }
  }

  /**
   * Narrow Facilities
   */
  private function facilityFilter($facility, $facility_filter) {

  }
}

<?php

namespace Drupal\connect2\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\connect2\Connect2Client;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class Connect2Config
 *
 * @package Drupal\connect2\Form
 */
class Connect2Config extends ConfigFormBase {

  /**
   * @var \Drupal\connect2\Connect2Client
   */
  protected $connect2Client;

  public function __construct($connect2_client) {
    $this->connect2Client = $connect2_client;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('connect2_client')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['connect2_facility_count.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'connect2_config';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $config = $this->config('connect2_facility_count.settings');

    $form['connect2']['account'] = [
      '#type' => 'textfield',
      '#title' => t('Connect 2 Account'),
      '#description' => t('Account ID give to the account within Connect 2.'),
      '#default_value' => $config->get('account'),
    ];

    $form['connect2']['url'] = [
      '#type' => 'textfield',
      '#title' => t('Connect 2'),
      '#description' => t('URL of the Connect 2 API.'),
      '#default_value' => $config->get('url'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory()->getEditable('connect2_facility_count.settings')
      ->set('account', $form_state->getValue('account'))
      ->set('url', $form_state->getValue('url'))
      ->save();

    \Drupal::messenger()->addMessage(t('Configuration has been saved.'));
  }
}
